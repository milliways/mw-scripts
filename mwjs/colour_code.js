// colourCode
// usage:
// 	colourCode.getNamedColourCode(colourName)
//		returns mw hex colour string code for colourName if it exists or '0' otherwise
//		I haven't really completed my intended function here so although code to live add  
//		named colours to this exists it's not yet persistent across sessions
//
//	colourCode.getRGBColourCode(r,g,b)
//		returns mw hex colour string closest to colour specified by floats r,g,b
//		r, g, b should be in range 0.0-1.0 but will be capped if not
//
//	colourCode.getIntRGBColourCode(r,g,b) or
//	colourCode.rgb(r,g,b)
//		returns mw hex colour string closest to colour specified by int r,g,b
//		r, g, b should be in range 0-255 but will be capped if not
//
//	colourCode.getHexRGBColourCode(hex_string) or
//	colourCode.hex(hex_string)
//		returns mw hex colour string closest to colour specified by string hex_string
//		hex_string should be in the form '#rrggbb', 'rrggbb' or short form '#rgb' or 'rgb'
//
// colourCode.getHSVColourCode(hue, saturation, value) or
// colourCode.hsv(hue, saturation, value)
// 	returns mw hex colour string closest to colour specified by the hsv triplet
//	hue should be in the range 0-360
//	saturation and value should be in the range 0.0 - 1.0
//
// This code requires String.padStart to exist (it doesn't currently in duk so you will
// require the polyfill (should be in the repo))

const colourCode = new function()
{
	var colours = { black: '10', 
					white: 'e7', 
	                red: 'c4', 
	                yellow: 'e2',
	                green: '2e',
	                cyan: '33',
	                blue: '15',
	                sucs_orange: 'a6'
	              };
	              
	var greys = ["10", "10", "e8", "e8", "e8", "e8", "e9", "e9", "e9", "e9", 
	             "ea", "ea", "ea", "eb", "eb", "eb", "eb", "ec", "ec", "ec", 
	             "ec", "ed", "ed", "ed", "ed", "ee", "ee", "ee", "ee", "ef", 
	             "ef", "ef", "ef", "f0", "f0", "f0", "3b", "3b", "f1", "f1", 
	             "f1", "f2", "f2", "f2", "f2", "f3", "f3", "f3", "f3", "f4", 
	             "f4", "f4", "66", "66", "f5", "f5", "f5", "f6", "f6", "f6", 
	             "f6", "f7", "f7", "f7", "f8", "f8", "f8", "f8", "91", "91", 
	             "f9", "f9", "fa", "fa", "fa", "fa", "fb", "fb", "fb", "fb", 
	             "fc", "fc", "fc", "bc", "bc", "fd", "fd", "fd", "fe", "fe", 
	             "fe", "fe", "ff", "ff", "ff", "ff", "ff", "e7", "e7", "e7", 
	             "e7"];
	             
	function getNamedColourCode(colourName)
	{
		if(colours[colourName] !== undefined)
		{
			return colours[colourName];
		}
		
		return '0';
	}
	this.getNamedColourCode = getNamedColourCode;
	
	function setNamedColour(colourName, colourCode)
	{
		colours[colourName] = colourCode;
	}
	this.setNamedColour = setNamedColour;
	
	function getRGBColourCode(r,g,b)
	{
		// limit r, g & b to 0.0 - 1.0
		r = Math.min( Math.max(r, 0.0), 1.0) 
		g = Math.min( Math.max(g, 0.0), 1.0) 
		b = Math.min( Math.max(b, 0.0), 1.0) 
		// scale r, g & b to an integer between 0 and 5
		var rr = Math.round( r * 5 );
		var gr = Math.round( g * 5 );
		var br = Math.round( b * 5 );
		// if rr, gr and br are the same we have a grey
		if(rr == gr && rr == br)
		{
			// there are 30 shades of grey
			// pick the closest using the greys array.
			var grey = Math.round((( r + g + b ) / 3.0) * 100);
			return greys[grey];
		}
		// otherwise use formula (36 * r) + (6 * g) + b + 16 to find the closest colour
		var ansiColourCode = ((36 * rr) + (6 * gr) + br + 16);
		// return the hex colour string padded with a 0 if required.
		return ansiColourCode.toString(16).padStart(2,'0');
	}
	this.getRGBColourCode = getRGBColourCode;
	
	function getIntRGBColourCode(r,g,b)
	{
		// simply divide the colours by 255.0 and call the float version to get the colour
		// no need to check for limits as float version does that
		return getRGBColourCode(r / 255.0, g / 255.0, b / 255.0);
	}
	this.getIntRGBColourCode = getIntRGBColourCode;
	this.rgb = getIntRGBColourCode;
	
	function getHexRGBColourCode(hex)
	{
		var rc='ff';
		var gc='ff';
		var bc='ff';
		if(typeof(hex) == 'string')
		{
			// regex for strings of format #rrggbb or rrggbb where rr gg & bb are hex pairs
			var regex = /^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i
			var hexMatch = regex.exec(hex);
			if(hexMatch !== null)
			{
				rc = hexMatch[1];
				gc = hexMatch[2];
				bc = hexMatch[3];
			}
			else
			{
				// regex for short hex string of format #rgb or rgb
				var shortRegex = /^#?([0-9a-f]{1})([0-9a-f]{1})([0-9a-f]{1})$/i
				var shortHexMatch = shortRegex.exec(hex);
				if(shortHexMatch !== null)
				{
					// make a long hex from the short hex
					rc = shortHexMatch[1] + shortHexMatch[1];
					gc = shortHexMatch[2] + shortHexMatch[2];
					bc = shortHexMatch[3] + shortHexMatch[3];
				}
			}
		}
		
		// turn the hex strings into ints and call the getIntRGB
		return getIntRGBColourCode(parseInt(rc,16), parseInt(gc,16), parseInt(bc,16));
	}
	this.getHexRGBColourCode = getHexRGBColourCode;
	this.hex = getHexRGBColourCode;
	
	function getHSVColourCode(h,s,v) 
	{    
		// takes h s v and turns them into a float r g b trio
		// pinched and made to work with duk from
		//   https://en.wikipedia.org/wiki/HSL_and_HSV#Alternative_HSV_to_RGB
		//   https://jsfiddle.net/Lamik/Lr61wqub/15/       
		function f(n)
		{
			var k=( n + h / 60 ) % 6;
			return v - v * s * Math.max( Math.min( k, 4-k , 1), 0 );
		}          
		return getRGBColourCode(f(5),f(3),f(1));  
	}
	this.getHSVColourCode = getHSVColourCode;
	this.hsv = getHSVColourCode;
} 
