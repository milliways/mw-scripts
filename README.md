# Milliways Scripts

A package of useful Milliways js library scripts.

## Installation

```
$ make install
```

To install to a non-default location, use

```
$ make DESTDIR=/non/default/location install
```
